use crate::player::Player;
use crate::weapon::Weapon;
use crate::GameEngine;

use crate::message::Message;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::{Arc, RwLock};

use rg3d::core::algebra::{UnitQuaternion, Vector3};
use rg3d::core::math::ray::Ray;
use rg3d::core::pool::{Handle, Pool};
use rg3d::engine::resource_manager::{MaterialSearchOptions, ResourceManager};
use rg3d::engine::Engine;
use rg3d::event::{DeviceEvent, ElementState, Event, VirtualKeyCode, WindowEvent};
use rg3d::event_loop::{ControlFlow, EventLoop};
use rg3d::gui::node::StubNode;
use rg3d::physics::{dynamics::RigidBodyBuilder, geometry::ColliderBuilder};
use rg3d::resource::texture::TextureWrapMode;
use rg3d::scene::base::BaseBuilder;
use rg3d::scene::camera::{CameraBuilder, SkyBox, SkyBoxBuilder};
use rg3d::scene::graph::Graph;
use rg3d::scene::node::Node;
use rg3d::scene::physics::RayCastOptions;
use rg3d::scene::transform::TransformBuilder;
use rg3d::scene::Scene;
use rg3d::window::WindowBuilder;

fn create_shot_trail(
    graph: &mut Graph,
    origin: Vector3<f32>,
    direction: Vector3<f32>,
    trail_length: f32,
) {
    let transform = TransformBuilder::new()
        .with_local_position(origin)
        .with_local_scale(Vector3::new(0.0025, 0.0025, trail_length))
        .with_local_rotation(UnitQuaternion::face_towards(&direction, &Vector3::y()))
        .build();

    // Create unit cylinder with caps that faces toward Z axis.
    let shape = Arc::new(RwLock::new(
        rg3d::scene::mesh::surface::SurfaceData::make_cylinder(
            6,     // Count of sides
            1.0,   // Radius
            1.0,   // Height
            false, // No caps are needed.
            // Rotate vertical cylinder around X axis to make it face towards Z axis
            &UnitQuaternion::from_axis_angle(&Vector3::x_axis(), 90.0f32.to_radians())
                .to_homogeneous(),
        ),
    ));

    rg3d::scene::mesh::MeshBuilder::new(
        BaseBuilder::new()
            .with_local_transform(transform)
            // Shot trail should live ~0.25 seconds, after that it will be automatically
            // destroyed.
            .with_lifetime(0.25),
    )
    .with_surfaces(vec![rg3d::scene::mesh::surface::SurfaceBuilder::new(shape)
        // Set yellow-ish color.
        .with_color(rg3d::core::color::Color::from_rgba(255, 255, 0, 120))
        .build()])
    // Do not cast shadows.
    .with_cast_shadows(false)
    // Make sure to set Forward render path, otherwise the object won't be
    // transparent.
    .with_render_path(rg3d::scene::mesh::RenderPath::Forward)
    .build(graph);
}

pub struct Game {
    scene: Handle<Scene>,
    pub(crate) player: Player,
    weapons: Pool<Weapon>,

    receiver: Receiver<Message>, // Single receiver, it cannot be cloned.
    _sender: Sender<Message>,    // Sender can be cloned and used from various places.
}

impl Game {
    pub async fn new(engine: &mut GameEngine) -> Self {
        let mut scene = Scene::new();

        // Load a scene resource and create its instance.
        engine
            .resource_manager
            .request_model(
                "data/models/scene.rgs",
                MaterialSearchOptions::MaterialsDirectory("data/textures".into()),
            )
            .await
            .unwrap()
            .instantiate_geometry(&mut scene);

        let (sender, receiver) = channel();

        let mut player =
            Player::new(&mut scene, engine.resource_manager.clone(), sender.clone()).await;
        let weapon = Weapon::new(&mut scene, engine.resource_manager.clone()).await;
        scene.graph.link_nodes(weapon.model(), player.weapon_pivot);
        let mut weapons = Pool::new();
        player.weapon = weapons.spawn(weapon);

        Self {
            _sender: sender,
            receiver,
            player,
            scene: engine.scenes.add(scene),
            weapons,
        }
    }

    pub fn update(&mut self, engine: &mut GameEngine, dt: f32) {
        let scene = &mut engine.scenes[self.scene];
        self.player.update(scene);
        // v New code v
        //
        for weapon in self.weapons.iter_mut() {
            weapon.update(dt, &mut scene.graph);
        }

        // We're using `try_recv` here because we don't want to wait until next message -
        // if the queue is empty just continue to next frame.
        while let Ok(message) = self.receiver.try_recv() {
            match message {
                Message::ShootWeapon { weapon } => {
                    self.shoot_weapon(weapon, engine);
                }
            }
        }
    }

    fn shoot_weapon(&mut self, weapon: Handle<Weapon>, engine: &mut GameEngine) {
        let weapon = &mut self.weapons[weapon];

        if weapon.can_shoot() {
            weapon.shoot();

            let scene = &mut engine.scenes[self.scene];

            let weapon_model = &scene.graph[weapon.model()];

            // Make a ray that starts at the weapon's position in the world and look toward
            // "look" vector of the weapon.
            let ray = Ray::new(
                scene.graph[weapon.shot_point()].global_position(),
                weapon_model.look_vector().scale(1000.0),
            );

            let mut intersections = Vec::new();

            scene.physics.cast_ray(
                RayCastOptions {
                    ray,
                    max_len: ray.dir.norm(),
                    groups: Default::default(),
                    sort_results: true, // We need intersections to be sorted from closest to furthest.
                },
                &mut intersections,
            );

            // Ignore intersections with player's capsule.
            let trail_length = if let Some(intersection) = intersections
                .iter()
                .find(|i| i.collider != self.player.collider)
            {
                //
                // TODO: Add code to handle intersections with bots.
                //

                // For now just apply some force at the point of impact.
                let collider = scene.physics.colliders.get(&intersection.collider).unwrap();
                scene
                    .physics
                    .bodies
                    .native_mut(collider.parent().unwrap())
                    .unwrap()
                    .apply_force_at_point(
                        ray.dir.normalize().scale(25.0),
                        intersection.position,
                        true,
                    );

                // Trail length will be the length of line between intersection point and ray origin.
                (intersection.position.coords - ray.origin).norm()
            } else {
                // Otherwise trail length will be just the ray length.
                ray.dir.norm()
            };

            create_shot_trail(&mut scene.graph, ray.origin, ray.dir, trail_length);
        }
    }
}
